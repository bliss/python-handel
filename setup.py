import sys
from setuptools import setup

TESTING = any(x in sys.argv for x in ['test', 'pytest'])

with open("README.md") as f:
    long_description = f.read()

setup(
    name='handel',
    version='0.1.2.dev0',

    packages=['handel'],
    install_requires=['cffi', 'numpy'],
    setup_requires=['pytest-runner', 'pytest'] if TESTING else [],
    tests_require=['pytest-cov', 'mock', 'gevent'],

    description="Python binding for the Handel library",
    long_description=long_description,

    license="GPLv3",
    author="Vincent Michel")
