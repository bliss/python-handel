import mock
import pytest


@pytest.fixture(scope='session')
def interface_import():
    with mock.patch('cffi.FFI.dlopen') as dlopen:
        with mock.patch('os.name', new='nt'):

            # Set up get version info to trigger a warning
            m = dlopen.return_value.xiaGetVersionInfo

            def side_effect(a, b, c, d):
                # v1.2.18 is too old!
                d[0], c[0], b[0], a[0] = b'v', 1, 2, 18
            m.side_effect = side_effect

            # Perform the import only once
            with pytest.warns(UserWarning) as record:
                from handel import interface
            assert len(record) == 1
            assert "older than 1.2.19" in str(record[0].message)

            # Remove mock
            del dlopen.return_value.xiaGetVersionInfo
            # Work around for https://bugs.python.org/issue31177
            dlopen.return_value._mock_children.clear()

            # Check dlopen
            dlopen.assert_called_once_with('handel.dll')
            assert interface.handel == dlopen.return_value

            yield interface


@pytest.fixture
def interface(interface_import):
    with mock.patch('handel.interface.check_error'):
        try:
            # Reset mock
            handel = interface_import.handel
            handel.reset_mock()
            yield interface_import
        finally:
            # Revert gevent patch
            interface_import.handel = handel
