
def test_gevent_compatibility(interface):
    # Declare xiaSomeFunction
    handel = interface.handel
    original = handel.xiaSomeFunction
    original.__name__ = 'xiaSomeFunction'
    original.return_value = 'Some result'

    # Patching
    from handel.gevent import patch
    assert patch() is None

    # Checking
    assert interface.handel.xiaSomeFunction.__name__ == 'xiaSomeFunction'
    assert interface.handel.xiaSomeFunction(1, a=2) is 'Some result'
    original.assert_called_once_with(1, a=2)
